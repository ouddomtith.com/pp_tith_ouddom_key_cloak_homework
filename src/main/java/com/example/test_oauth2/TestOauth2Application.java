package com.example.test_oauth2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class TestOauth2Application {

    @GetMapping
    public void get(){

    }

    public static void main(String[] args) {
        SpringApplication.run(TestOauth2Application.class, args);
    }

}
